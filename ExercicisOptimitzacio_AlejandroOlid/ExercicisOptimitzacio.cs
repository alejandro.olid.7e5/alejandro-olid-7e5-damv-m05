﻿/* Alumno: Alejandro Olid
   Descripción: Ejercicios metodos, parametros y tests.
   Fecha: 20/01/2022 */

using System;
namespace ExercicisOptimitzacio_AlejandroOlid
{
    public static class ExercicisOptimitzacio
    {
        public static bool IsLeapYear(int? year)
        {
            Console.WriteLine("El año introducido es: {0}",year);
            bool leapyear;
            
            if ( year % 4 == 0 && year % 100 != 0) leapyear = true;
            else if (year % 100 == 0 && year % 400 == 0) leapyear = true;
            else leapyear = false;
            
            if (leapyear) Console.WriteLine ("Es bisiesto.");
            else Console.WriteLine ("No es bisiesto.");
            
            return leapyear;
        }

        public static void MinOf10Values(int[] vector)
        {
            if (vector != null)
            {
                var min = vector[0];
                foreach (var i in vector)
                {
                    if (min > i) min = i;
                }

                Console.WriteLine("El minimo en el vector es: {0}", min);
            }
            else Console.WriteLine("No se han encontrado datos.");
        }

        public static void HowManyDays(int? year, int? month)
        {
            if (year != null && month != null)
            {
                var leapyear = IsLeapYear(year);
                Console.WriteLine("El mes introducido es: {0}", month);
                switch (month)
                {
                    case 1:
                    case 3:
                    case 5:
                    case 7:
                    case 8:
                    case 10:
                    case 12:
                    {
                        month = 31;
                        break;
                    }
                    case 2:
                        if (leapyear)
                        {
                            month = 29;
                            break;
                        }
                        else
                        {
                            month = 28;
                            break;
                        }
                    case 4:
                    case 6:
                    case 9:
                    case 11:
                    {
                        month = 30;
                        break;
                    }
                }

                Console.Write("Dias del mes: {0}", month);
            }
            else Console.WriteLine("No se han encontrado datos.");
        }

        public static void Dni(string dni)
        {
            Console.WriteLine("DNI: {0}", dni);
            if (dni != null && dni.Length == 9)
            {
                string vector = null;
                var letra1 = '\0';
                char letra2;
                for (var i = 0; i < 8; i++)
                {
                    vector += dni[i];
                }

                var m = Convert.ToInt32(vector);
                var n = m % 23;
                letra1 = n switch
                {
                    0 => 'T',
                    1 => 'R',
                    2 => 'W',
                    3 => 'A',
                    4 => 'G',
                    5 => 'M',
                    6 => 'Y',
                    7 => 'F',
                    8 => 'P',
                    9 => 'D',
                    10 => 'X',
                    11 => 'B',
                    12 => 'N',
                    13 => 'J',
                    14 => 'Z',
                    15 => 'S',
                    16 => 'Q',
                    17 => 'V',
                    18 => 'H',
                    19 => 'L',
                    20 => 'C',
                    21 => 'K',
                    22 => 'E',
                    _ => letra1
                };
                letra2 = dni[8];

                if (letra1 == letra2) Console.Write("Es un DNI válido");
                else Console.Write("No es un DNI válido");
            }
            else Console.Write("No es un DNI válido");
        }
        
        public static void Main()
        {
        }
    }
}