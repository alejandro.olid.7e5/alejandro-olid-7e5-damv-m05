﻿/* Alumno: Alejandro Olid
   Descripción: Ejercicios metodos, parametros y tests.
   Fecha: 20/01/2022 */

using ExercicisOptimitzacio_AlejandroOlid;
using NUnit.Framework;
namespace TestProject1
{
    public class TestMinOf10Values
    {
        [Test]
        public void MinOf10Values_ShouldBe3()
        {
            int[] vector = {312, 4, 13, 312, 23, 312, 33, 3, 333, 412};
            ExercicisOptimitzacio.MinOf10Values(vector);
        }
        
        [Test]
        public void MinOf10Values_ShouldBeNegative()
        {
            int[] vector = {312, -4, 13, 312, 23, 312, 33, 3313, 333, 412};
            ExercicisOptimitzacio.MinOf10Values(vector);
        }
        
        [Test]
        public void MinOf10Values_Null()
        {
            ExercicisOptimitzacio.MinOf10Values(null);
        }
    }
}