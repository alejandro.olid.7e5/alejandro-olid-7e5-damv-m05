﻿/* Alumno: Alejandro Olid
   Descripción: Ejercicios metodos, parametros y tests.
   Fecha: 20/01/2022 */

using ExercicisOptimitzacio_AlejandroOlid;
using NUnit.Framework;
namespace TestProject1
{
    public class TestDni
    {
        [Test]
        public void Dni_Valid()
        {
            var dni = "20070457J";
            ExercicisOptimitzacio.Dni(dni);
        }
        
        [Test]
        public void Dni_Invalid()
        {
            var dni = "25270457J";
            ExercicisOptimitzacio.Dni(dni);
        }
        
        [Test]
        public void Dni_Empty()
        {
            var dni = "";
            ExercicisOptimitzacio.Dni(dni);
        }
        
        [Test]
        public void Dni_DistinctOf8()
        {
            var dni = "2313123331221";
            ExercicisOptimitzacio.Dni(dni);
        }
        
        [Test]
        public void Dni_Null()
        {
            ExercicisOptimitzacio.Dni(null);
        }
    }
}