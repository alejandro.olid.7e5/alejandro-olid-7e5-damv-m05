﻿/* Alumno: Alejandro Olid
   Descripción: Ejercicios metodos, parametros y tests.
   Fecha: 20/01/2022 */

using ExercicisOptimitzacio_AlejandroOlid;
using NUnit.Framework;
namespace TestProject1
{
    public class TestHowManyDays
    {
        [Test]
        public void HowManyDays_ShouldBe31()
        {
            var year = 2022;
            var month = 1;
            ExercicisOptimitzacio.HowManyDays(year, month);
        }
        
        [Test]
        public void HowManyDays_ShouldBe30()
        {
            var year = 2022;
            var month = 4;
            ExercicisOptimitzacio.HowManyDays(year, month);
        }
        
        [Test]
        public void HowManyDays_LeapYear()
        {
            var year = 1600;
            var month = 2;
            ExercicisOptimitzacio.HowManyDays(year, month);
        }
        
        [Test]
        public void HowManyDays_NotLeapYear()
        {
            var year = 2022;
            var month = 2;
            ExercicisOptimitzacio.HowManyDays(year, month);
        }
        
        [Test]
        public void HowManyDays_Null()
        {
            ExercicisOptimitzacio.HowManyDays(null, null);
        }
    }
}